#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<arpa/inet.h>
#include<time.h>

#define SERVER_PORT 12344
#define BUF_SIZE 4096

int main(int argc, char* argv[])
{
	int c, s;
	char buf[BUF_SIZE];
	char resbuf[BUF_SIZE];
	struct hostent* h;
	struct sockaddr_in channel;

	if(argc != 2)
		printf("client use format: client,choose\n");

	s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(s < 0 )
		printf("client create socket fail!\n");
	
	memset(&channel, 0, sizeof(channel));
	channel.sin_family = AF_INET;
	channel.sin_port = htons(SERVER_PORT);
	inet_pton(AF_INET, "127.0.0.1", &channel.sin_addr);

	c = connect(s, (struct sockaddr *)&channel, sizeof(channel));
	if(c < 0 )
		printf("client connect fail\n");

	char QUERY_BUF[2048] = {0};
	//select login
	char* query1 = "1-gyz-1234";
	char* query2 = "2-szb2-123456";
	if(strcmp(argv[1],"1") == 0)
		strcat(QUERY_BUF, query1);
	else
		strcat(QUERY_BUF, query2);
	
	fprintf(stdout,"query:\n%s\n",QUERY_BUF);
	write(s, QUERY_BUF, 2048);
	while(1){
		int bytes = read(s, resbuf, BUF_SIZE);
		if(bytes <= 0)
			break;
	}
	fprintf(stdout,"receive from logic server:\n%s\n",resbuf);
	close(s);
	return 0;
}
