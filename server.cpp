#include<sys/types.h>
#include<sys/fcntl.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<memory.h>
#include<string.h>
#include"mysql.h"
#include"cJSON.h"

#define SERVER_PORT 12345
#define BUF_SIZE 4096
#define QUEUE_SIZE 10

int main(int argc, char* argv[])
{
	//initial database and connect
	const char* host = "125.216.242.29";
	const char* user = "wx";
	const char* pass = "wx4rb";
	const char* db = "wx";
	MYSQL mysql;
	MYSQL_RES *result;
	MYSQL_ROW row;

	mysql_init(&mysql);
	if(!mysql_real_connect(&mysql,host,user,pass,db,0,NULL,0)){
		printf("%s",mysql_error(&mysql));
	}
	else{
		printf("success connected!\n");
	}

	//create socket and listen
	int s, b,l, sa, on = 1;
	char buf[BUF_SIZE];
	char resbuf[BUF_SIZE];
	struct  sockaddr_in channel;

	memset(&channel, 0, sizeof(channel));
	channel.sin_family = AF_INET;
	channel.sin_addr.s_addr = htonl(INADDR_ANY);
	channel.sin_port = htons(SERVER_PORT);
	
	s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(s < 0 )
		printf("storage server create socket fail!");
	setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on));

	b = bind(s, (struct sockaddr*)&channel, sizeof(channel));
	if(b < 0 )
		printf("storage server bind fail!");

	l = listen(s, QUEUE_SIZE);
	if(l < 0 )
		printf("storage server listen fail!");

	while(1){		
		sa = accept(s, 0, 0);
		if(sa < 0 )
			printf("storage server accept fail!");
		read(sa, buf, BUF_SIZE);
		printf("storage server get the massage from client:%s\n",buf);
		
		cJSON *root;
		//select query
		if(buf[0] == 's')
		{
			printf("select query in storage level..\n");
			mysql_query(&mysql,buf);
			result = mysql_store_result(&mysql);
			int num = 0;
			char tmp[BUF_SIZE];
			char res_num[8] = {0};
			MYSQL_FIELD* fields;
			cJSON *info;
			root = cJSON_CreateObject();
			cJSON_AddNumberToObject(root, "result", result->row_count);
			cJSON_AddItemToObject(root, "info", info=cJSON_CreateArray());
			int field_num = mysql_num_fields(result);
			fields = mysql_fetch_fields(result);
			while((row = mysql_fetch_row(result)))
			{
				cJSON *rowjs;
				cJSON_AddItemToArray(info,rowjs=cJSON_CreateObject());
				for(int i = 0; i< field_num; i++)
				{
					cJSON_AddStringToObject(rowjs, fields[i].name, row[i]);
				}	
			}		
			mysql_free_result(result);
		}
		//insert delete update query
		else
		{
			printf("insert,delete,update query in storage level..\n");
			int res_query = mysql_query(&mysql, buf);
			root = cJSON_CreateObject();
			if(!res_query){
				cJSON_AddNumberToObject(root, "result", 0);
				cJSON_AddNumberToObject(root, "affect", mysql_affected_rows(&mysql));
			}
			else{
				cJSON_AddNumberToObject(root, "result", -1);
				cJSON_AddStringToObject(root, "msg", mysql_error(&mysql));
			}
		}
		char* out=cJSON_Print(root);	
		cJSON_Delete(root);		
		strcat(resbuf,out);
		free(out);
		write(sa, resbuf, BUF_SIZE);
		close(sa);
		memset(resbuf, 0, BUF_SIZE);
	}
	return 0;
}











	
